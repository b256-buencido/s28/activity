
// Insert a single room using insertOne method
db.rooms.insertOne({
    "name": "single",
    "accommodates": 2,
    "price": 1000,
    "description": "A single room with all the basic necessities",
    "rooms_available": 10,
    "isAvailable": false,
});

// Insert multiple rooms using insertMany method
db.rooms.insertMany([
    {
        "name": "double",
        "accommodates": 3,
        "price": 2000,
        "description": "A room fit for a small family going on a vacation",
        "rooms_available": 5,
        "isAvailable": false,
    },
    {
        "name": "queen",
        "accommodates": 4,
        "price": 4000,
        "description": "A room with a queen sized bed perfect for a single getaway",
        "rooms_available": 15,
        "isAvailable": false,
    }
]);


// Use the find method to search for a room with the name double 
db.rooms.find({"name": "double"});

// Use the updateOne method to update the queen room and set the available rooms to 0
db.rooms.updateOne(
    {"name": "queen"},
    {
        $set: {
            "rooms_available": 0,
        }
    }
    
);

// Use the find method to check if the update has been done 
db.rooms.find({"name": "queen"});

// Use the deleteMany method to delete all rooms that have 0 rooms available 
db.rooms.deleteMany({"rooms_available": 0});

// Use the find method to check and display all the rooms offered in the hotel
db.rooms.find();